function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
$(function() {
    var id = getUrlParameter('id');
    switch(id) {
        case '1':
                $("#name").text('ANPR IR Lamp DSP color camera')
                $("#code").text('CPT-SHR150')
                $("#type").text('ANPR')
                $("#description").text('-Super H-IR Integrated IR illuminator LPC camera, good at night\n-High Definition1080P\n-With IR, good at night\n-Long life MTBF 10,000 hours\n-Weather proof IP68 grade')
                $('#img_path').attr("src","images/product/CPT-SHR600HD.jpg")
                $('#doc_path').css("display","none")
          break;
        case '2':
                $("#name").text('High Speed ANPR IR Lamp DSP color camera')
                $("#code").text('CPT-SHR750')
                $("#type").text('ANPR')
                $("#description").text('-Super H-IR Integrated IR illuminator LPC camera, good at night \n-High Definition 1280x1024@30fps \n-With IR, good at night \n-Long life MTBF 10,000 hours \n-Weather proof IP68 grade')
                $('#img_path').attr("src","images/product/CPT-SHR600HD.jpg")
                $('#doc_path').css("display","none")
            break;
        case '3':
                $("#name").text('High sensitivity for ANPR IR Lamp DSP color camera')
                $("#code").text('CPT-SHR600HD3')
                $("#type").text('ANPR')
                $("#description").text('-Super H-IR Integrated IR illuminator LPC camera,Full day to identification 150KM plate number over 98%\n-Full High Definition 1080P@60fps\n-Long life MTBF 20,000 hours\n-Weather proof IP68 grade')
                $('#img_path').attr("src","images/product/CPT-SHR600HD.jpg")
                $('#doc_path').attr("href","specs/CPT-SHR600HD3-en.pdf")
                break;
        case '4':
                $("#name").text('Defog Lens')
                $("#code").text('CPT-LNS-NIR10330D-2MAF')
                $("#type").text('Defog and HDR')
                $("#description").text('-2 Mega Full HD 1920 x 1080 \n-Defog filter for haze environment\n-Auto Focus or Motorized Focus\n- Spectrum 420nm ~ 1105nm near IR range')
                $('#img_path').attr("src","images/product/defog-len.jpg")
                $('#doc_path').attr("href","specs/Catalog-NIR 33X Lens_CPT-LNS-NIR10330D-2MAF_en_spec.pdf")
                break;
        case '5':
                $("#name").text('Defog Lens')
                $("#code").text('CPT-LNS-NIR10330D-3MA')
                $("#type").text('Defog and HDR')
                $("#description").text('-3 Mega Full HD 1920 x 1080 \n-Defog filter for haze environment\n-Auto Focus or Motorized Focus\n- Spectrum 420nm ~ 1105nm near IR range')
                $('#img_path').attr("src","images/product/defog-len2.jpg")
                $('#doc_path').attr("href","specs/Catalog-NIR 60X Lens_CPT-LNS-NIR125750D-3MAF_en_spec.pdf")
                break;
        case '6':
                $("#name").text('Defog Camera')
                $("#code").text('CPT-IPC-8100B')
                $("#type").text('Defog and HDR')
                $("#description").text('- 2Mega Full HD 1920 x 1080\n-Support H.265 & H.264 \n-Defog filter for haze environment and HDR processing\n- Auto back focus(ABF) and 16X digital Zoom\n-Speed Shutter Range Designed. Control Shutter Range')
                $('#img_path').attr("src","images/product/defog-camera.jpg")
                $('#doc_path').attr("href","specs/Chiper_IPC_Defog_Srarvis_CPT-IPC-8100B-DF_en.pdf")
                break;
        case '7':
                $("#name").text('Pressure & Dust Explosion‐Proof Stainless Steel IR camera Housing')
                $("#code").text('CPT-EXH1840')
                $("#type").text('Explosion-Proof')
                $("#description").text('-application: refinery, refuel, gas station, mine, sewage treatment, thermal power, wafer, biologic, chemical, sea area, combustible, anti - salt damage, corrosion - resistant and explosion- proof environment.\n-Weatherproof standard IEC529 and IP-68 protection grade\n-built in heater and automatic sensor control\n-Compliant to IEC 61241,IECEx/ILAC/IAF/MLA Ex-proof Standard\n-Compliant to CNS3376-0(2008)、CNS3376-1(2008) Ex-proof Standard')
                $('#img_path').attr("src","images/product/CPT-EXH1840-IR.jpg")
                $('#doc_path').css("display", "none");
                break;
        case '8':
                $("#name").text('Explosion-proof & weather-proof AISI304L Stainless steel Housing')
                $("#code").text('CPT-EXH1546')
                $("#type").text('Explosion-Proof')
                $("#description").text('-All stainless steel material\n-Weatherproof standard IEC529 and IP-68 protection grade\n-Compliant to IEC 61241,IECEx/ILAC/IAF/MLA Ex-proof Standard\n- Compliant to CNS3376-0(2008)、CNS3376-1(2008) Ex-proof Standard')
                $('#img_path').attr("src","images/product/CPT-EXH1546.jpg")
                $('#doc_path').attr("href","specs/CPT-EXH1546-en.pdf")
                break;
        case '9':
                $("#name").text('Palm Vein')
                $("#code").text('PalmVEIN')
                $("#type").text('Biometrics')
                $("#description").text('-Non-touch palm vein blood detection\n-Fast and accurate\n-The identification rate is high. FRR = 0.01%, FAR = 0.00008% \n-Anti-DDOS/ACL')
                $('#img_path').attr("src","images/product/palmvein.jpg")
                $('#doc_path').attr("href","specs/Catalog_Biological_PalmVEIN_CPT-NPV301_en.pdf")
                break;
        case '10':
                $("#name").text('Indoor Pan/Tilt Driver')
                $("#code").text('CPT-AP111T-100')
                $("#type").text('Pan Tilt')
                $("#description").text('-Auto Pan feature, Tilt during pan auto scan\n-Limit switch on Pan & tilt\n-Fire-proof VL cable 6pin detachable\n-ABS/BRASS\n-Die-Casting Aluminum & ABS+FRP, color Ivory')
                $('#img_path').attr("src","images/product/CPT-AP111T-100.jpg")
                $('#doc_path').attr("href","specs/CPT-AP111T-en.pdf")
                break;
        case '11':
                $("#name").text('Weather Proof Heavy duty Pan/Tilt Driver')
                $("#code").text('CPT-AP112T-100')
                $("#type").text('Pan Tilt')
                $("#description").text('-Weather proof standard IP-55.\n-Pan rotation angle: 0° ~ 355°, Tilt: ±95°°\n-Hardened steel gear, for longest durability\n-Rubber ring & gasket seals for all weather environmental protection\n-Heavy duty Loading, available for other peripherals I.e. IR lamp etc')
                $('#img_path').attr("src","images/product/CPT-AP112T-100.jpg")
                $('#doc_path').attr("href","specs/CPT-AP112T-en.pdf")
                break;
        case '12':
                $("#name").text('Weather Proof Heavy duty Pan/Tilt Driver')
                $("#code").text('CPT-AP115T')
                $("#type").text('Pan Tilt')
                $("#description").text('-Weather proof standard IP-55\n-Pan rotation angle: 0° ~ 355°, Tilt: ±95°\n-Hardened steel gear, for longest durability\n-Heavy duty Loading, available for other peripherals I.e. IR lamp etc.\n-Rubber ring & gasket seals for all weather environmental protection')
                $('#img_path').attr("src","images/product/AP115T.jpg")
                $('#doc_path').attr("href","specs/CPT-AP115T-en.pdf")
                break;
        case '13':
                $("#name").text('Weather Proof Pan/Tilt Driver')
                $("#code").text('CPT-PT35')
                $("#type").text('Pan Tilt')
                $("#description").text('-Weather proof standard IP-67\n-Available with optional preset function\n-Optional Variable speed function\n-Heavy duty Loading, available for other peripherals I.e. IR lamp etc\n-Upright installation 38 Kg/84Lb')
                $('#img_path').attr("src","images/product/PT25.jpg")
                $('#doc_path').attr("href","specs/CPT-PT35-en.pdf")
                break;
        case '14':
                $("#name").text('Weather Proof Pan/Tilt Driver')
                $("#code").text('CPT-PT30')
                $("#type").text('Pan Tilt')
                $("#description").text('-Weather proof standard IP-67\n-Available with optional preset function\n-Optional Variable speed function\n-Heavy duty Loading, available for other peripherals I.e. IR lamp etc\n-Upright installation 35 Kg/77Lb')
                $('#img_path').attr("src","images/product/PT30.jpg")
                $('#doc_path').attr("href","specs/CPT-PT30-en.pdf")
                break;
        case '15':
                $("#name").text('Weather Proof AISI304L Stainless steel Pressurized Housing')
                $("#code").text('CPT-HS701-12N')
                $("#type").text('Explosion-Proof')
                $("#description").text('-All stainless steel material\n-Weatherproof standard IP-68 protection grade\n-Application installation in marine, chemical, food processing plants, highly corrosive area, industrial environment')
                $('#img_path').attr("src","images/product/CPT-HS701-12N-100.jpg")
                $('#doc_path').attr("href","specs/CPT-HS701-12N-en.pdf")
                break;
        case '16':
                $("#name").text('Weather Proof AISI304L Stainless steel Pressurized Housing')
                $("#code").text('CPT-HS701-12N-IR')
                $("#type").text('Explosion-Proof')
                $("#description").text('-All stainless steel material\n-Weatherproof standard IP-68 protection grade\n-Distance: 35meter, DC12V 15W\n-Application installation in marine, chemical, food processing plants, highly corrosive area, industrial environment')
                $('#img_path').attr("src","images/product/CPT-HS701-12N-IR-100.jpg")
                $('#doc_path').attr("href","specs/CPT-HS701-12N-IR-en.pdf")
                break;
        case '17':
                $("#name").text('Weather Proof AISI316L Stainless steel Pressurized Housing')
                $("#code").text('CPT-HS701-N316')
                $("#type").text('Explosion-Proof')
                $("#description").text('-All stainless steel material\n-Weatherproof standard IP-68 protection grade\n-Application installation in marine, chemical, food processing plants, highly corrosive area, industrial environment')
                $('#img_path').attr("src","images/product/CPT-HS701-N316-100.jpg")
                $('#doc_path').attr("href","specs/CPT-HS701-N316-en.pdf")
                break;
        case '18':
                $("#name").text('Weather Proof Stainless steel Nitrogen pressurize-able Housing')
                $("#code").text('CPT-HS701-20N')
                $("#type").text('Explosion-Proof')
                $("#description").text('-All stainless steel material\n-Weatherproof standard IP-68 protection grade\n-Application installation in marine, chemical, food processing plants, highly corrosive area, industrial environment')
                $('#img_path').attr("src","images/product/18.png")
                $('#doc_path').attr("href","specs/CPT-HS701-20N-en.pdf")
                break;
        case '19':
                $("#name").text('Streamlined style Housing')
                $("#code").text('CPT-HS701-10')
                $("#type").text('Explosion-Proof')
                $("#description").text('-IEC60529 International standard\n- Use aluminum SUS304 material\n- Use of Ge Glass (optional)\n-Defog & heating (optional)\n-Wiper & reservoir (optional)')
                $('#img_path').attr("src","images/product/19.png")
                $('#doc_path').css("display", "none");
                break;
        case '20':
                $("#name").text('Heavy Duty Bracket Mount')
                $("#code").text('CPT-BK702-7')
                $("#type").text('Accessory')
                $("#description").text('-Max loading: 500kg\n-CPT-BK-702-7T .511Lx230Wx230Hmm- CPT-BK-702-7I .510Lx230Wx230Hmm- 4.2kg (Stainless Iron)0Hmm')
                $('#img_path').attr("src","images/product/20.png")
                $('#doc_path').attr("href","specs/Catalog_Bracket_BK702-7T_7I-en.pdf")
                break;
        case '21':
                $("#name").text('Heavy Duty Bracket Mount')
                $("#code").text('CPT-BK702-7S')
                $("#type").text('Accessory')
                $("#description").text('-Max loading: 200kg\n-CPT-BK-702-7ST .511Lx230Wx230Hmm- 1.6kg (Stainless Steel)\n-CPT-BK-702-7SI .510Lx230Wx230Hmm- 1.25kg (Stainless Iron)')
                $('#img_path').attr("src","images/product/21.png")
                $('#doc_path').attr("href","specs/Catalog_Bracket_BK702-7ST_7SI-en.pdf")
                break;
        default:
          // code block
      }
});